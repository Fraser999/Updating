# Updating Example

This is an example of a possible way to handle continuous updates to production code in the various MaidSafe projects to allow the final Vault and Client products to remain stable across different versions of the binaries.

[![build status](https://gitlab.com/ci/projects/18167/status.png?ref=master)](https://gitlab.com/ci/projects/18167?ref=master)
