extern crate cbor;
extern crate updating;

#[test]
fn check_info() {
    println!("");
    ::updating::v0_8::info();
    ::updating::v0_9::info();
    ::updating::v1_0::info();
    println!("");
}

#[test]
fn check_immutable_data() {
    println!("");

    // Version 0.8
    println!("Oldest version is {}", ::updating::v0_8::version());

    let v0_8_imm_data = ::updating::v0_8::ImmutableData::new(false, "oldest immutable data".to_string());
    println!("  Using new -- {:?}\n", v0_8_imm_data);

    // Version 0.9
    println!("Middle version is {}", ::updating::v0_9::version());

    let v0_9_imm_data = ::updating::v0_9::ImmutableData::new(::updating::v0_9::ImmutableDataType::Normal, "older immutable data".to_string());
    println!("  Using new -- {:?}", v0_9_imm_data);

    let v0_9_from_v0_8_imm_data = ::updating::v0_9::ImmutableData::from(v0_8_imm_data.clone());
    println!("  From v0.8 -- {:?}", v0_9_from_v0_8_imm_data);

    let v0_9_into_v0_8_imm_data: ::updating::v0_8::ImmutableData = v0_9_from_v0_8_imm_data.into();
    println!("  Into v0.8 -- {:?}\n", v0_9_into_v0_8_imm_data);

    // Current versions
    println!("Latest version is {}", ::updating::v1_0::version());

    let v1_0_imm_data = ::updating::v1_0::ImmutableData::new(::updating::v1_0::ImmutableDataType::Backup, "latest immutable data".to_string());
    println!("  Using new -- {:?}", v1_0_imm_data);

    let mut v1_0_from_v0_8_imm_data = ::updating::v1_0::ImmutableData::from(v0_8_imm_data);
    println!("  From v0.8 -- {:?}", v1_0_from_v0_8_imm_data);

    let v1_0_from_v0_9_imm_data = ::updating::v1_0::ImmutableData::from(v0_9_imm_data);
    println!("  From v0.9 -- {:?}", v1_0_from_v0_9_imm_data);

    // Changing to v0.8 converts type from Backup to Sacrificial since v0.8 doesn't have Backup type
    v1_0_from_v0_8_imm_data.type_tag = ::updating::v1_0::ImmutableDataType::Backup;
    let v1_0_into_v0_8_imm_data: ::updating::v0_8::ImmutableData = v1_0_from_v0_8_imm_data.into();
    println!("  Into v0.8 -- {:?}", v1_0_into_v0_8_imm_data);
    assert_eq!(::updating::v0_8::ImmutableDataType::Sacrificial, v1_0_into_v0_8_imm_data.type_tag);

    let v1_0_into_v0_9_imm_data: ::updating::v0_9::ImmutableData = v1_0_from_v0_9_imm_data.into();
    println!("  Into v0.9 -- {:?}\n", v1_0_into_v0_9_imm_data);

    println!("");
}

#[test]
fn check_structured_data() {
    println!("");

    // Version 0.8
    let v0_8_strd_data = ::updating::v0_8::StructuredData{ type_tag: 2, data: "oldest structured data".to_string(), version: 0, };
    println!("{:?}\n", v0_8_strd_data);

    // Version 0.9
    let v0_9_strd_data = ::updating::v0_9::StructuredData{ type_tag: 5, data: "older structured data".to_string(), version: 11, };
    println!("{:?}\n", v0_9_strd_data);

    // Current versions
    let v1_0_strd_data = ::updating::v1_0::StructuredData{ type_tag: 10, data: "latest structured data".to_string(), version: 22, };
    println!("{:?}\n", v1_0_strd_data);

    println!("");
}
