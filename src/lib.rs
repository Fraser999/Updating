extern crate rustc_serialize;

pub mod v0_8;
pub mod v0_9;
pub mod v1_0;

#[test]
fn check_versions() {
    assert_eq!(concat!(env!("CARGO_PKG_VERSION_MAJOR"), '.', env!("CARGO_PKG_VERSION_MINOR")), v1_0::version());
    assert_eq!("0.9", v0_9::version());
    assert_eq!("0.8", v0_8::version());
}
