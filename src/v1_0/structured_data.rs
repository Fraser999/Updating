#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, RustcEncodable, RustcDecodable)]
pub struct StructuredData {
    pub type_tag: u64,
    pub data: String,
    pub version: u64,
}

impl ::std::fmt::Debug for StructuredData {
    fn fmt(&self, formatter: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(formatter, "StructuredData v{} [type_tag: {}, data: \"{}\", version: {}]", super::version(), self.type_tag, self.data, self.version)
    }
}
