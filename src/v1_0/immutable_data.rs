pub use ::v0_9::ImmutableDataType as ImmutableDataType;



#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, RustcEncodable, RustcDecodable)]
pub struct ImmutableData {
    pub type_tag: ImmutableDataType,
    pub value: String,
    name: String,
}

impl ImmutableData {
    pub fn new(type_tag: ImmutableDataType, value: String) -> ImmutableData {
        ImmutableData{ type_tag: type_tag, value: value, name: "v1_0 name".to_string(), }
    }

    pub fn name(&self) -> String {
        self.name.clone()
    }
}

impl ::std::fmt::Debug for ImmutableData {
    fn fmt(&self, formatter: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(formatter, "ImmutableData v{} [name(): \"{}\", type_tag: {:?}, value: \"{}\"]",
               super::version(), self.name, self.type_tag, self.value)
    }
}

// Conversion to v0.8
impl ::std::convert::Into<::v0_8::ImmutableData> for ImmutableData {
    fn into(self) -> ::v0_8::ImmutableData {
        ::v0_8::ImmutableData{ type_tag: self.type_tag.into(), value: self.value, }
    }
}

// Conversion from v0.8
impl ::std::convert::From<::v0_8::ImmutableData> for ImmutableData {
    fn from(older_version: ::v0_8::ImmutableData) -> ImmutableData {
        ImmutableData{ type_tag: ImmutableDataType::from(older_version.type_tag),
                       value: older_version.value,
                       name: "v1_0 name from v0_8 instance".to_string(), }
    }
}

// Conversion to v0.9
impl ::std::convert::Into<::v0_9::ImmutableData> for ImmutableData {
    fn into(self) -> ::v0_9::ImmutableData {
        ::v0_9::ImmutableData{ type_tag: self.type_tag, value: self.value, }
    }
}

// Conversion from v0.9
impl ::std::convert::From<::v0_9::ImmutableData> for ImmutableData {
    fn from(older_version: ::v0_9::ImmutableData) -> ImmutableData {
        ImmutableData{ type_tag: older_version.type_tag,
                       value: older_version.value,
                       name: "v1_0 name from v0_9 instance".to_string(), }
    }
}
