pub fn info() {
    println!("This is a function defined in v0_8");
}

pub fn get_major_version(module_path: &str) -> u8 {
    version_part(1, module_path)
}

pub fn get_minor_version(module_path: &str) -> u8 {
    version_part(0, module_path)
}

pub fn get_version(major: u8, minor: u8) -> String {
    format!("{}.{}", major, minor)
}

fn version_part(index: usize, module_path: &str) -> u8 {
    module_path.rsplitn(3, |c| c == ':' || c == '_' || c == 'v').collect::<Vec<&str>>()[index].parse::<u8>().unwrap()
}
