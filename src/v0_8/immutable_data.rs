#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, RustcEncodable, RustcDecodable, Debug)]
pub enum ImmutableDataType {
    Normal,
    Sacrificial,
}



#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, RustcEncodable, RustcDecodable)]
pub struct ImmutableData {
    pub type_tag: ImmutableDataType,
    pub value: String,
}

impl ImmutableData {
    pub fn new(is_normal: bool, value: String) -> ImmutableData {
        let type_tag = if is_normal {
            ImmutableDataType::Normal
        } else {
            ImmutableDataType::Sacrificial
        };
        ImmutableData{ type_tag: type_tag, value: value, }
    }

    pub fn name(&self) -> String {
        "v0_8 name".to_string()
    }
}

impl ::std::fmt::Debug for ImmutableData {
    fn fmt(&self, formatter: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(formatter, "ImmutableData v{} [name(): \"{}\", type_tag: {:?}, value: \"{}\"]",
               super::version(), self.name(), self.type_tag, self.value)
    }
}
