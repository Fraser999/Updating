pub use self::immutable_data::*;
pub use self::structured_data::*;
pub use self::utils::*;

pub fn version_major() -> u8 {
    get_major_version(module_path!())
}

pub fn version_minor() -> u8 {
    get_minor_version(module_path!())
}

pub fn version() -> String {
    get_version(version_major(), version_minor())
}

mod immutable_data;
mod structured_data;
mod utils;
