#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, RustcEncodable, RustcDecodable, Debug)]
pub enum ImmutableDataType {
    Normal,
    Backup,
    Sacrificial,
}

// Conversion to v0.8
impl ::std::convert::Into<::v0_8::ImmutableDataType> for ImmutableDataType {
    fn into(self) -> ::v0_8::ImmutableDataType {
        match self {
            ImmutableDataType::Normal => ::v0_8::ImmutableDataType::Normal,
            ImmutableDataType::Backup => ::v0_8::ImmutableDataType::Sacrificial,
            ImmutableDataType::Sacrificial => ::v0_8::ImmutableDataType::Sacrificial,
        }
    }
}

// Conversion from v0.8
impl ::std::convert::From<::v0_8::ImmutableDataType> for ImmutableDataType {
    fn from(older_version: ::v0_8::ImmutableDataType) -> ImmutableDataType {
        match older_version {
            ::v0_8::ImmutableDataType::Normal => ImmutableDataType::Normal,
            ::v0_8::ImmutableDataType::Sacrificial => ImmutableDataType::Sacrificial,
        }
    }
}



#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, RustcEncodable, RustcDecodable)]
pub struct ImmutableData {
    pub type_tag: ImmutableDataType,
    pub value: String,
}

impl ImmutableData {
    pub fn new(type_tag: ImmutableDataType, value: String) -> ImmutableData {
        ImmutableData{ type_tag: type_tag, value: value, }
    }

    pub fn name(&self) -> String {
        "v0_9 name".to_string()
    }
}

impl ::std::fmt::Debug for ImmutableData {
    fn fmt(&self, formatter: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(formatter, "ImmutableData v{} [name(): \"{}\", type_tag: {:?}, value: \"{}\"]",
               super::version(), self.name(), self.type_tag, self.value)
    }
}

// Conversion to v0.8
impl ::std::convert::Into<::v0_8::ImmutableData> for ImmutableData {
    fn into(self) -> ::v0_8::ImmutableData {
        ::v0_8::ImmutableData{ type_tag: self.type_tag.into(), value: self.value, }
    }
}

// Conversion from v0.8
impl ::std::convert::From<::v0_8::ImmutableData> for ImmutableData {
    fn from(older_version: ::v0_8::ImmutableData) -> ImmutableData {
        ImmutableData{ type_tag: ImmutableDataType::from(older_version.type_tag),
                       value: older_version.value, }
    }
}
