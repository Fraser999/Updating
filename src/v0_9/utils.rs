pub fn info() {
    println!("This is a function defined in v0_9");
}

pub use ::v0_8::get_major_version as get_major_version;
pub use ::v0_8::get_minor_version as get_minor_version;
pub use ::v0_8::get_version as get_version;
